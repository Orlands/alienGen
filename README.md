# alienGen

## About the project
This is a project I made for alienGen, the project is a subscription panel of different offer.
You can choose either the 2022 EXCLUSIVE OFFER or the regular offer. 

> **WARNING**: This project was realized according to a perfect pixel mockup so the size of render is a bit different from the image sent. The 20022 EXCLUSIVE OFFER was displayed only on screen with large size.

## Usage

The integration of this code snippet can be displayed on the homepage of a website. It will let the users to have a view of the differents offer an pricing of the solution.

![AlienGenImg](/readme/img/alienGen.jpg "Optional Title")


### Built With

* [Nuxt.js](https://nuxtjs.org/)
* [Vue.js](https://vuejs.org/)
* [Bootstrap-vue](https://bootstrap-vue.org/)

## Getting Started

### Installation

```bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm run start

# generate static project
$ npm run generate
```

